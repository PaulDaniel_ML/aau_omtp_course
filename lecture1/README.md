## Building a Robot Simulation Environment in ROS

Author: Paul Daniel

Lecture 1 introduced URDF and XACRO as tools to build robot simulation environments for ROS. 

In order to get familiar with these tools, the file *omtp_factory.xacro* was modified.
Changes include:

* Removal of unneeded bins
* Moving of bin 1 to different location
* Created custum xacro macro *insert_shape.xacro*. This macro allows the user to insert an object with a shape, position, color and name of choice.
* Included *insert_shape.xacro* in *omtp_factory.xacro*, used it to create a red sphere and a green cylinder
* Added a UR5-Robot to the world, including a new pedestal
* Added an ABB IRB 6640 robot to the world

Factory world initial state: 

![Factory old](/lecture1/images/factory_old.png)  

Factory world after all changes:  

![Factory new](/lecture1/images/factory_new.png)  