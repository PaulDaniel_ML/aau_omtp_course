![AAU Logo](/lecture1/images/AAU_logo.png)

## Repository for the AAU Course "Object manipulation and task planning" - Spring 2020

Author: Paul Daniel

Each subfolder contains the submissions for one of the lecture assignments. A seperate README-file is available in each folder,
with a more detailed description.

Overview of lecture contents:

* Lecture 1: 
	* Building a Robot Simulation Environment in ROS
		* URDF
		* XACRO
	* Version Control & Collaboration with Git



